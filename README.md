# Firebeans

Der inoffizielle Rocketbeans TV Programmplan für Firefox, Chrome & Opera, inkl. einfachem Zugriff zu diversen nützlichen Dingen rund um die Rocket Beans


**Achtung:** Diese Dokumentation ist noch unvollständig

**Kompatibilität**: Inzwischen ist Firebeans nur noch ab Firefox 45 lauffähig, da auf die WebExtension-API umgestellt wurde


## Voraussetzungen
### Libraries/Programme
* Node.js

Damit die verschiedenen Versionen der Erweiterungen automatisch generiert werden können, wird Gulp benutzt, dementsprechend wird zur Entwicklung der Extension Node vorrausgesetzt.

Mehr zur [Installation der notwendigen Tools weiter unten](#installation).

Mehr zum [Build-Prozess weiter unten](#build-prozess).

### Google API Key
Damit die YouTube-Funktionen des Addons funktionieren, muss in der Datei `src/lib/module-youtube.js` ein API-Key in der Variable `key` eingetragen werden. Es gibt leider keine Möglichkeit, diesen Key zu verbergen oder zu verschlüsseln, aufgrund der Technologie von Browser-Extension wird dieser immer lesbar in der Extension stehen, da Extension nicht kompiliert oder verschlüsselt werden.

Der Zugriff auf die YouTube-API reicht, es müssen keine anderen Google-Dienste mehr für diesen Key aktiviert werden (früher war für Firebeans noch die Google Calendar API wichtig).

**To Do:** Damit der API-Key nicht ausversehen im Repository landet, sollte noch eine Prozedur geschrieben werden, die diesen Key aus einer nicht im Repository befindlichen Konfigurationsdatei einspeist.

### Firefox
Firefox unterstützt ab Version 45 fast alle Extension-APIs von Chrome, deswegen wird der Firebeans Chrome-Quellcode für beide Varianten benutzt. Firefox benötigt aber noch einen Eintrag in der `manifest.json`, deswegen finden sich zwei getrennte `manifest_*.json`-Dateien für die beiden Plattformen im `/src`-Verzeichnis.

Es ist wichtig, dass in der `manifest_firefox.json` eine **eindeutige** ID unter applications.gecko.id angegeben wird, Firefox (und das Mozilla-Addon-Einreich-Gremium) wird sich weigern, das Addon anzunehmen, falls dieser Eintrag fehlt.

```json
"applications": {
	"gecko": {
		"id": "dein-projekt-name@dein-name",
		"strict_min_version": "45.0.0"
	}
}
```


## Installation der notwendigen Dateien
Damit alle notwendigen Pakete und Bibliotheken installiert werden, muss zunächst in der Kommandozeile im Root-Verzeichnis des Plugins `npm install` und danach `bower install` ausgeführt werden. Node.JS ist zwingend dafür notwendig, falls noch nicht installiert muss über den Befehl `npm install bower` noch die Paketverwaltung Bower installiert werden.


## Build-Prozess
Um lauffähige Versionen der Erweiterungen für die verschiedenen Browser zu erzeugen, wird Gulp genutzt. Im `dist`-Verzeichnis erstellt das Skript jeweils für Firefox und Chrome einen Ordner, in den die jeweiligen kompilierten Dateien kopiert werden, die dann theoretisch aus diesem Ordner heraus als Extension gepackt werden könnten.

### Automatisch (alle Plattformen)
Um automatisch jedes Mal, wenn Dateien im Quelltext geändert werden, den Build-Prozess zu starten (was zum Beispiel praktisch ist, um die Erweiterung mit diesen erzeugten Dateien direkt im Browser zu testen), rufe einfach das Kommando `gulp` über die Kommando-Zeile im Root-Verzeichnis des Extension-Quellcodes (also nicht in `/src` oder `/dist`) auf. Gulp wird dann bei jeder Änderungen den Build-Prozess starten. Die fertigen Dateien finden sich dann im `/dist/` Verzeichnis.

**Achtung!** Der Build-Prozess wird erst bei der ersten Änderung gestartet, beim allerersten Start sollte der Build-Proess manuell ausgeführt werden (oder eben eine Datei geändert werden).

### Manuell
Der Buildprozess kann auch manuell gestartet werden, hierbei kann bei Bedarf auch eine Ziel-Plattform definiert werden.

* **Build-Kommando für alle Plattformen:** `gulp build`
* **Build-Kommando für Firefox:** `gulp build --firefox`
* **Build-Kommando für Chrome:** `gulp build --chrome`


## Ja, toll, und wie krieg ich die Scheiße jetzt in meinen Browser?!
Firefox und Chrome sind etwas picky, was die Installation von Addons aus fremden Quellen (aka nicht von Google oder Mozilla abgesegnete Addons) angeht. Das Addon kann nur im Entwicklungsmodus den Browsern hinzugefügt werden, das ist aber bei beiden nicht so kompliziert. Leider bedeutet das für Firefox, dass das Addon bei jedem Neustart des Browsers neu hinzugefügt werden muss. Das ist nervig — geht aber leider nicht anders.

### Chrome
Rufe die Chrome-interne URL `chrome://extensions` im Browser auf. Aktiviert oben rechts die Checkbox _"Developer Mode"_ (oder die deutsche Equivalente). Jetzt könnt ihr über den Button "Load unpacked extension" den vorher erstellten Ordner `/dist/chrome` auswählen. Das Addon sollte nun in eurer Browser-Leiste erscheinen

### Firefox
Rufe die Firefox-interne URL `about:debugging` auf. Aktiviert die Checkbox _"Enable add-on debugging"_ (bzw. die deutsche Equivalente). Über den Button _"Load Temporary Add-on"_ (oder halt das deutsche dafür) könnt ihr im zuvor erstellten Ordern `/dist/firefox/` die Datei `manifest.json` auswählen. Nun sollte das Addon in eurer Browser-Leiste erscheinen.

Wie gesagt, wird das Addon bei jedem Neustart des Browsers gelöscht. Cache-Daten bleiben evtl. erhalten, aber es ist nervig, dass das Addon bei jedem Beenden des Firefox verschwindet. Das ist leider eine Design-Entscheidung von Mozilla, die nicht geändert werden kann — außer man lässt das Addon von Mozilla signieren.

## Veröffentlichen
Damit die Addons normal installiert werden können, müssen sie jeweils im Chrome Store und im Mozilla Addon Directory veröffentlich werden.

Der Chrome-Store verlangt dafür einen Google Account und eine einmalige Überweisungen von ein paar Cent per Kreditkarte zur Identifikation.

Bei Mozilla müssen alle Addons und ihre Updates eingereicht werden, damit die einmal drüberschauen und die Datei signieren.

Beide Prozeduren sind nervig (gerade weil Mozilla häufig ähnlich wie Apple das Addon auch mal wegen abstrusen Problemen rejected), sind aber zu vertreten. Beides ist recht gut in den jeweiligen Dokus der Hersteller dokumentiert.

[Veröffentlichungs-Anleitung für Firefox](https://developer.mozilla.org/de/Add-ons/Distribution)
[Veröffentlichungs-Anleitung für Chrome](https://developer.chrome.com/webstore/publish)