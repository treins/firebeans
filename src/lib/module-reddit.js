/*
 *
 *	Modul, das die letzten Reddit-Posts holts
 *
 */

Reddit = function() {
	this.getLatestPosts = function(category,callback) {
		var requestURL = "https://www.reddit.com/r/rocketbeans/"+category+".json?sort=new";

		var xhr = new XMLHttpRequest();
		xhr.open("GET", requestURL, true);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				if (typeof callback !== "undefined") {
					var jaaasooon = JSON.parse(xhr.responseText);
					var items = jaaasooon.data.children;
					callback(items);
				}
			}
		}
		xhr.send();

	}
}