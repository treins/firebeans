function getMonday(targetDate) {
	var monday = "";
	// get the start day of the targetDate's week
	targetDate = new Date(targetDate);
	targetDate.setMinutes(0);
	targetDate.setHours(0);
	targetDate.setSeconds(0);
	targetDate.setMilliseconds(0);

	if ((targetDate.getDay()) == 1) { monday = targetDate; }
	if ((targetDate.getDay()) == 0) {
		// sunday
		monday = targetDate;
		monday.setDate( targetDate.getDate()-6);
	}
	else {
		var difference = (targetDate.getDay()-1)
		monday = targetDate;
		monday.setDate( targetDate.getDate()-difference );
	}
	return monday;
}


function timeDifference(current, previous, lang) {
	if (typeof lang == "undefined") { lang = "en"; }

	var msPerMinute = 60 * 1000;
	var msPerHour = msPerMinute * 60;
	var msPerDay = msPerHour * 24;
	var msPerMonth = msPerDay * 30;
	var msPerYear = msPerDay * 365;

	var elapsed = current - previous;

	if (elapsed < msPerMinute) {
		var value = Math.round(elapsed/1000);
		if (lang == "en") { return value + 's'; }
		if (lang == "de" && value !== 1) { return value + ' Sekunden'; }
		if (lang == "de") { return value + ' Sekunde'; }
	}

	else if (elapsed < msPerHour) {
		var value = Math.round(elapsed/msPerMinute);
		if (lang == "en") { return value + 'm'; }
		if (lang == "de" && value !== 1) { return value + ' Minuten'; }
		if (lang == "de") { return value + 'Minute'; }
	}

	else if (elapsed < msPerDay ) {
		var value = Math.round(elapsed/msPerHour );
		if (lang == "en") { return value + 'h'; }
		if (lang == "de" && value !== 1) { return value + ' Stunden'; }
		if (lang == "de") { return value + ' Stunde'; }
	}

	else if (elapsed < msPerMonth) {
		var value = Math.round(elapsed/msPerDay);
		if (lang == "en") { return value+ 'd'; }
		if (lang == "de" && value !== 1) { return value + ' Tagen'; }
		if (lang == "de") { return value + ' Tag'; }
	}

	else if (elapsed < msPerYear) {
		var value = Math.round(elapsed/msPerMonth);
		if (lang == "en") { return value + 'mo'; }
		if (lang == "de" && value !== 1) { return value + ' Monaten'; }
		if (lang == "de") { return value + ' Monat'; }
	}

	else {
		var value = Math.round(elapsed/msPerYear );
		if (lang == "en") { return value + 'y'; }
		if (lang == "de" && value !== 1) { return value + ' Jahren'; }
		if (lang == "de") { return value + ' Jahr'; }
	}
}


function debugLog(message) {
	console.log(message);
}