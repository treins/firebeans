/*
 *
 *	Modul für den offiziellen Sendeplan der Bohnen
 *
 */

 var schedule_url = "http://api.rbtv.rodney.io/api/1.0/schedule/schedule.json";


Sendeplan = function() {

	// -- Get Week --
	// Get all available events from the server, sorted by weekdays
	this.getWeek = function(callback) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", schedule_url, true);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				try{
					var weekSchedule = JSON.parse(xhr.responseText);

					if (typeof weekSchedule.schedule === "undefined") {
						throw false;
					}

					if (typeof callback !== "undefined") {
						callback(weekSchedule);
					}
				}
				catch(e) {
					callback();
				}
			}
		}
		xhr.send();
	}

	// -- Update Cache: --
	// Get the current week schedule from the server and add, update and remove
	// cached events (or create a cache if no cache exists upon calling this method)
	// Returns the updated cache.
	this.updateCache = function(callback) {
		var sendeplan = this;

		// check if a cache exists
		chrome.storage.local.get("eventCache",function(result) {

			// initiate the cache if it didn't exist before
			if (typeof result.eventCache === "undefined") {
				chrome.storage.local.set(
					{
						"eventCache" : {
							events: [],
							updated: new Date().getTime(),
							created: new Date().getTime()
						}
					},
					function() {
						getAndSaveEvents();
					}
				);
			}
			else {
				getAndSaveEvents();
			}
		});

		function getAndSaveEvents() {
			// get the current local cache first
			chrome.storage.local.get("eventCache",function(cacheObject) {
				// get the latest week schedule from the server
				sendeplan.getWeek(function(remoteEvents) {
					if (typeof remoteEvents !== "undefined") {
						if (typeof cacheObject.eventCache !== "undefined") {
							if (typeof cacheObject.eventCache.events !== "undefined") {
								var cachedEvents = cacheObject.eventCache.events;
								var remoteScheduleDays = remoteEvents.schedule;

								var newEventCache = cacheObject.eventCache;
								newEventCache.events = []; // remote the current cache
								newEventCache.updated = new Date().getTime();

								// iterate through each day in the schedule fetched from the RBTV API
								for (var prop in remoteScheduleDays) {
									if (remoteScheduleDays.hasOwnProperty(prop)) {
										var events = remoteScheduleDays[prop];

										// iterate through this day's events
										events.forEach(function(event) {
											// add event to an array containing all events that should be present in the new week schedule
											newEventCache.events.push(event);

										});
									}
								}

								chrome.storage.local.set({"eventCache":newEventCache},function() {
									if (typeof callback !== "undefined") {
										callback(newEventCache);
									}
								});
							}
						}
					}
				});
			});
		}
	}

	// -- Get Cached Events: --
	// Get locally cached events
	// Options: {sort:[_none_|date},filter:[none|range] default: none
	this.getCachedEvents = function(callback,options) {
		chrome.storage.local.get("eventCache",function(cacheObject) {
			var events = [];
			var updated = false;

			if (typeof cacheObject.eventCache !== "undefined") {
				if (typeof cacheObject.eventCache.events !== "undefined") {
					
					// parse options
					var parsedOptions = {
						"sort": "none",
						"filter": "none"
					}

					if (typeof options !== "undefined") {
						if (typeof options.sort === "string") {
							parsedOptions.sort = options.sort;
						}
						if (typeof options.filter === "string") {
							parsedOptions.filter = options.filter;
						}
					}

					if (parsedOptions.sort == "none") {
						events = cacheObject.eventCache.events;
					}

					if (parsedOptions.filter == "range" || typeof options.range !== "undefined") {
						var filteredEvents = [];

						if (typeof options.range[0] !== "undefined" && typeof options.range[1]) {
							var start = new Date(options.range[0]);
							var end = new Date(options.range[1]);

							events.forEach(function(event) {
								var timeStart = new Date(event.timeStart);
								if (timeStart >= start && timeStart < end) {
									filteredEvents.push(event);
								}
							});
						}

						events = filteredEvents;
					}

					if (typeof cacheObject.eventCache.updated !== "undefined") {
						updated = cacheObject.eventCache.updated;
					}
				}
			}
			callback({
				events: events,
				updated: updated
			});
		});
	}

	// -- Get Current Event (Cached) --
	this.getCurrentEvent = function(callback) {
		chrome.storage.local.get("eventCache",function(cacheObject) {
			var currentEvent = {};
			if (typeof cacheObject.eventCache !== "undefined") {
				if (typeof cacheObject.eventCache.events !== "undefined") {
					var cachedEvents = cacheObject.eventCache.events;
					var now = new Date();

					cachedEvents.forEach(function(event) {
						var start = new Date(event.timeStart);
						var end = new Date(event.timeEnd);

						if (start <= now && end >= now) {
							currentEvent = event;
						}
					});
				}
			}

			if (typeof callback !== "undefined") {
				callback(currentEvent);
			}
		});
	}
}