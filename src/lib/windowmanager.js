var WindowManager = function() {
	this.target = "";

	this.initiate = function(target) {
		if (typeof target !== "undefined") {
			this.target = target;
		}
	}

	this.openPage = function(pageInfo,targetSubpage) {
		// Zunächst wird in das popup.html ein div für die Seite erstellt, falls noch keine existiert. Hierein können dann alle Daten und Elemente für die Seite geschrieben werden
		if ($(".window#"+pageInfo.page,this.target).length < 1) {
			var pageBody = $("<div id='"+pageInfo.page+"' class='window active'></div>");

			// Zunächst wird der Header der Seite hinzugefügt, der den Seitentitel und die Unterseiten Navigation enthält
			if (typeof pageInfo.hideHeader === "undefined" || pageInfo.hideHeader !== true) {

				// erst wird das template geladen
				var headerTemplate = this.loadTemplate("templatepart-header.html");
				headerTemplate = $(headerTemplate);

				// dann wird der seiten-titel an der entsprechenden stelle eingesetzt
				$(".window-title",headerTemplate).text(pageInfo.title);

				// falls unterseiten definiert wurden, erstellen wir auch ein unterseiten-menü
				if (typeof pageInfo.subpages !== "undefined") {
					$(".window-menu",headerTemplate).removeClass("hide");
					pageInfo.subpages.forEach(function(subPage) {
						var item = '<li data-switcher-target="'+subPage.name+'" data-switcher-parent="'+pageInfo.page+'">'+subPage.title+'</li>';
						$(".window-menu",headerTemplate).append(item);
					});
				}

				$(pageBody).append(headerTemplate);
			}
			$(this.target).append(pageBody);
		}
		
		$(".window",this.target).removeClass("active"); // allen seiten die klasse 'active' entziehen

		$(".window#"+pageInfo.page,this.target).addClass("active"); // nur diese seite bekommt dann die klasse 'active'
		$(".window#"+pageInfo.page+" .window-content",this.target).remove(); // bereits bestehende inhalte, die von einem vorherigen seiten-aufbau  stammen könnten, werden entfern
		$(".window#"+pageInfo.page+" .window-title",this.target).text(pageInfo.title); // der seiten-titel wird erneut eingesetzt

		// falls unterseiten definiert wurden, aktuelle unterseite als aktiv markieren
		if (typeof pageInfo.subpages !== "undefined") {
			var subpage = false;
			if (typeof targetSubpage === "undefined") {
				subpage = pageInfo.subpages[0].name;
			}
			else {
				subpage = targetSubpage;
			}


			$(".window#"+pageInfo.page+" .window-menu li").removeClass("active");
			$(".window#"+pageInfo.page+" .window-menu li[data-switcher-target="+subpage+"]").addClass("active");
		}
	}

	this.loadSubPage = function(pageInfo,subPageName,insertData) {
		if (typeof insertData !== "undefined") {
			$(".window.active .window-menu li[data-switcher-target='"+subPageName+"']",this.target).addClass("active");
			insertData();
		}
	}

	// wenn wir templates laden, benutzen wir diese funktion (sie funktioniert in Chrome & Firefox)
	this.loadTemplate = function(templateName) {
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("GET", "../popup/templates/"+templateName, false);
		xmlhttp.send();
		return xmlhttp.responseText;
	}

}