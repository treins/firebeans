FirebeansOptions = function() {
	this.initiateOptions = function(callback) {
		chrome.storage.local.get("options",function(result) {

			// return option values if options were already initiated
			if (typeof result.options !== "undefined") {
				if (typeof callback !== "undefined") {
					callback(result.options);
				}
			}

			// load possible options and set default values
			else {
				// Load possible options
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4) {
						var response = JSON.parse(xhr.responseText);
						var possibleOptions = response.options;

						var options = [];

						possibleOptions.forEach(function(possibleOption) {
							options.push({
								name: possibleOption.name,
								value: possibleOption.defaultValue
							});
						});

						console.log(options);
						
						chrome.storage.local.set({
							"options": options
						}, function() {
							if (typeof callback !== "undefined") {
								callback(options);
							}
						});
					}
				}
				xhr.open("GET", chrome.extension.getURL('/options/options.json'), true);
				xhr.send();
			}
		});
	}

	this.getOptionValue = function(optionName,callback) {
		this.initiateOptions(function(options) {
			options.forEach(function(option) {
				if (option.name == optionName && typeof option.value != "undefined") {
					callback(option.value);
				}
			});
		});
	}

	this.getAllOptionValues = function(callback) {
		this.initiateOptions(function(options) {
			if (typeof options !== "undefined" && typeof callback !== "undefined") {
				callback(options)
			}
		});
	}
}