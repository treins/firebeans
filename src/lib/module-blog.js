Feed = function() {
	this.getFeed = function(callback) {
		var xmlRequest = new XMLHttpRequest();

		xmlRequest.open("GET","http://rocketbeans.tv/blog/feed/");

		xmlRequest.onreadystatechange = function (aEvt) {
	  		if (xmlRequest.readyState == 4) {
				if(xmlRequest.status == 200) {
					var items = xmlRequest.responseXML.documentElement.querySelectorAll("item");
					//console.log( items[0].querySelectorAll["title"] );

					var blogItems = [];

					for (var i=0; i<items.length; i++ ) {
						var itemInfo = {
							title: false,
							description: false,
							link: false,
							pubDate: false,
							guid: false
						};
						
						for (var j=0; j<items[i].childNodes.length; j++) {
							var node = items[i].childNodes[j];
							switch (node.nodeName) {
								case "title":
									itemInfo.title = node.textContent;
									break;
								case "link":
									itemInfo.link = node.textContent;
									break;
								case "pubDate":
									itemInfo.pubDate = node.textContent;
									break;
								case "description":
									itemInfo.description = node.textContent;
									break;
								case "content:encoded":
									itemInfo.content = node.textContent;
								case "guid":
									itemInfo.guid = node.textContent;
									break;
							}
						}

						// remove the last paragraph, since it's totally clear that the content is loaded from the Rocket Beans blog
						if (itemInfo.description !== false) {
							if (itemInfo.title !== false) {
								itemInfo.description = itemInfo.description.replace('<p>Der Beitrag <a rel="nofollow" href="'+itemInfo.link+'">'+itemInfo.title+'</a> erschien zuerst auf <a rel="nofollow" href="http://www.rocketbeans.tv">Rocket Beans TV</a>.</p>',"blaaa");
							}
						}

						// get the first picture in the blog post, if the blog post has pictures
						if (itemInfo.content !== false) {
							var m,
							picUrls = [];
							str = itemInfo.content;
							rex =  /<img.*?src="([^">]*\/([^">]*?))".*?>/g;;

							while ( m = rex.exec( str ) ) {
								picUrls.push( m[1] );
							}

							if (picUrls.length > 0) {
								itemInfo.pic = picUrls[0];
							}
						}

						blogItems.push(itemInfo);
					}

					if (typeof callback !== "undefined") {
						callback(blogItems);
					}
				}
			}
		};

		xmlRequest.send(null);
	}
}